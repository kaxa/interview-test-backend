import { ContactService } from "./contact.service";
import { AddContactModel } from "./contanct.models";
export declare class ContactController {
    private contactService;
    constructor(contactService: ContactService);
    listContact(): Promise<any>;
    addContact(addContactModel: AddContactModel): Promise<any>;
    validateContactForm(addContactModel: AddContactModel): void;
}
