"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactController = void 0;
const common_1 = require("@nestjs/common");
const contact_service_1 = require("./contact.service");
let ContactController = class ContactController {
    constructor(contactService) {
        this.contactService = contactService;
    }
    async listContact() {
        return await this.contactService.listContacts();
    }
    async addContact(addContactModel) {
        this.validateContactForm(addContactModel);
        return await this.contactService.addContact({
            birthDate: addContactModel.birthDate,
            comment: addContactModel.comment,
            email: addContactModel.email,
            name: addContactModel.name,
            position: addContactModel.position,
            surName: addContactModel.surName
        });
    }
    validateContactForm(addContactModel) {
        if (!addContactModel.name)
            throw new common_1.BadRequestException("Missing name");
        if (!addContactModel.surName)
            throw new common_1.BadRequestException("Missing surName");
        if (!addContactModel.birthDate)
            throw new common_1.BadRequestException("Missing birthDate");
        if (!addContactModel.email)
            throw new common_1.BadRequestException("Missing email");
        if (!addContactModel.comment)
            throw new common_1.BadRequestException("Missing comment");
        if (!addContactModel.position)
            throw new common_1.BadRequestException("Missing position");
    }
};
__decorate([
    (0, common_1.Get)(""),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ContactController.prototype, "listContact", null);
__decorate([
    (0, common_1.Post)(""),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ContactController.prototype, "addContact", null);
ContactController = __decorate([
    (0, common_1.Controller)("contact"),
    __metadata("design:paramtypes", [contact_service_1.ContactService])
], ContactController);
exports.ContactController = ContactController;
//# sourceMappingURL=contact.controller.js.map