import { PrismaService } from "../prisma.service";
import { Prisma } from "@prisma/client";
export declare class ContactService {
    private prisma;
    constructor(prisma: PrismaService);
    listContacts(): Promise<import(".prisma/client").Contact[]>;
    addContact(data: Prisma.ContactCreateInput): Promise<import(".prisma/client").Contact>;
}
