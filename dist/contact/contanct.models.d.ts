export interface AddContactModel {
    name: string;
    surName: string;
    email: string;
    position: string;
    birthDate: string;
    comment: string;
}
