import { BadRequestException, Body, Controller, Get, Post } from "@nestjs/common";
import { ContactService } from "./contact.service";
import { AddContactModel } from "./contanct.models";

@Controller("contact")
export class ContactController {

  constructor(private contactService: ContactService) {
  }

  @Get("")
  async listContact(): Promise<any> {
    return await this.contactService.listContacts();
  }

  @Post("")
  async addContact(@Body() addContactModel: AddContactModel): Promise<any> {
    this.validateContactForm(addContactModel);
    return await this.contactService.addContact({
      birthDate: addContactModel.birthDate,
      comment: addContactModel.comment,
      email: addContactModel.email,
      name: addContactModel.name,
      position: addContactModel.position,
      surName: addContactModel.surName
    });
  }

  validateContactForm(addContactModel: AddContactModel) {
    if (!addContactModel.name) throw new BadRequestException("Missing name");
    if (!addContactModel.surName) throw new BadRequestException("Missing surName");
    if (!addContactModel.birthDate) throw new BadRequestException("Missing birthDate");
    if (!addContactModel.email) throw new BadRequestException("Missing email");
    if (!addContactModel.comment) throw new BadRequestException("Missing comment");
    if (!addContactModel.position) throw new BadRequestException("Missing position");
  }

}
