import { Injectable } from "@nestjs/common";
import { PrismaService } from "../prisma.service";
import { Prisma } from "@prisma/client";

@Injectable()
export class ContactService {
  constructor(private prisma: PrismaService) {
  }

  async listContacts() {
    return this.prisma.contact.findMany();
  }



  async addContact(data: Prisma.ContactCreateInput) {
    return this.prisma.contact.create({
      data: data
    });
  }

}
